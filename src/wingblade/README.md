# WingBlade
WingBlade is an abstraction layer targeting multiple JavaScript runtimes simultaneously. No TypeScript support is considered.

Projects utilizing WingBlade are expected to write web-oriented platform-agnostic code. For the most part, unless a better measure is found, WingBlade adheres to the Deno API scheme.

## API
* [System](system.md)
* [Filesystem](file.md)
* [Utilities](util.mjs)
* [Networking](net.md)
* [Web services](web.md)