# General MIDI Extended
GME, short for General MIDI Extended, is a standard based off General MIDI level 1, which emphasizes on slightly extending the ability of General MIDI while not being too demanding. General MIDI Extended can be considered as a summarization of the common functionality among all GM-compatible synthesizers.

## Requirements
Any GME capable module must support full stereo sound, has at least 24-note polyphony, supports 16-part multi-timbrality, and adheres any additional requirements listed below.

* [Voice list](voice.md)
* [Drum note mapping](drum.md)
* [Events, CC and RPN](ch.md)
* [System Exclusive](sysex.md)
