# Voice list
## Melodic voices
### Piano
| PC# | Voice Name           |
| --- | -------------------- |
| 000 | Grand Piano          |
| 001 |  |
| 002 |  |
| 003 |  |
| 004 |  |
| 005 |  |
| 006 |  |
| 007 |  |

### Chromatic percussion
| PC# | Voice Name           |
| --- | -------------------- |
| 008 |  |
| 009 |  |
| 010 |  |
| 011 |  |
| 012 |  |
| 013 |  |
| 014 |  |
| 015 |  |

### Organ
| PC# | Voice Name           |
| --- | -------------------- |
| 016 |  |
| 017 |  |
| 018 |  |
| 019 |  |
| 020 |  |
| 021 |  |
| 022 |  |
| 023 |  |

### Guitar
| PC# | Voice Name           |
| --- | -------------------- |
| 024 |  |
| 025 |  |
| 026 |  |
| 027 |  |
| 028 |  |
| 029 |  |
| 030 |  |
| 031 |  |

### Bass
| PC# | Voice Name           |
| --- | -------------------- |
| 032 |  |
| 033 |  |
| 034 |  |
| 035 |  |
| 036 |  |
| 037 |  |
| 038 |  |
| 039 |  |

### Solo strings/orchestra
| PC# | Voice Name           |
| --- | -------------------- |
| 040 |  |
| 041 |  |
| 042 |  |
| 043 |  |
| 044 |  |
| 045 |  |
| 046 |  |
| 047 |  |

### Ensemble strings/orchestra
| PC# | Voice Name           |
| --- | -------------------- |
| 048 |  |
| 049 |  |

### Brass
| PC# | Voice Name           |
| --- | -------------------- |

### Reed
| PC# | Voice Name           |
| --- | -------------------- |

### Pipe
| PC# | Voice Name           |
| --- | -------------------- |

### Synth lead
| PC# | Voice Name           |
| --- | -------------------- |
| 080 |  |
| 081 |  |
| 082 |  |
| 083 |  |
| 084 |  |
| 085 |  |
| 086 |  |
| 087 |  |

### Synth pad
| PC# | Voice Name           |
| --- | -------------------- |
| 088 | New Age Pad          |
| 089 | Warm Pad             |
| 090 | Poly Synth Pad       |
| 091 | Choir Pad            |
| 092 | Bowed Glass Pad      |
| 093 | Metal Pad            |
| 094 | Halo Pad             |
| 095 | Sweep Pad            |

### Synth melodic sound effects
| PC# | Voice Name           |
| --- | -------------------- |
| 096 | Ice Rain             |
| 097 | Soundtrack           |
| 098 | Crystal              |
| 099 | Atmosphere           |
| 100 | Brightness           |
| 101 | Goblins              |
| 102 | Echo Drops           |
| 103 | Sci-Fi               |

### Ethnic
| PC# | Voice Name           |
| --- | -------------------- |
| 104 | Sitar                |
| 105 | Banjo                |
| 106 | Shamisen             |
| 107 | Koto                 |
| 108 | Kalimba              |
| 109 | Bagpipe              |
| 110 | Fiddle               |
| 111 | Shanai               |

### Percussive
| PC# | Voice Name           |
| --- | -------------------- |
| 112 | Tinkle Bell          |
| 113 | Agogo                |
| 114 | Steel Drums          |
| 115 | Woodblock            |
| 116 | Taiko Drum           |
| 117 | Melodic Tom          |
| 118 | Synth Drum           |
| 119 | Reverse Cymbal       |

### Sound effects
| PC# | Voice Name           |
| --- | -------------------- |
| 120 | Guitar Fret Noise    |
| 121 | Breath Noise         |
| 122 | Seashore             |
| 123 | Bird Tweet           |
| 124 | Telephone Ring       |
| 125 | Helicopter           |
| 126 | Applause             |
| 127 | Gunshot              |